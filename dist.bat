::-----------------------------------------------------------------------------
:: This script copies all the files you need to get started into build/dist
::-----------------------------------------------------------------------------
@echo off
setlocal
set BUILD_DIR=build
set DIST_DIR=%BUILD_DIR%\dist

if "%1"=="clean" goto clean

if not exist %DIST_DIR% md %DIST_DIR%

::-----------------------------------------------------------------------------
:dist

xcopy /Q/Y   root %DIST_DIR%\

xcopy /Q/Y/S modele %DIST_DIR%\modele\
xcopy /Q/Y   pandoc\pandoc.latex %DIST_DIR%\pandoc\
xcopy /Q/Y   scripts %DIST_DIR%\scripts\

xcopy /Q/Y   src\md\include.tex %DIST_DIR%\src\md\
xcopy /Q/Y   src\md\main\modele-vierge %DIST_DIR%\src\md\

xcopy /Q/Y   src\tex\include.tex %DIST_DIR%\src\tex\
xcopy /Q/Y   src\tex\main\modele-vierge %DIST_DIR%\src\tex\

goto end
::-----------------------------------------------------------------------------
:clean

echo . Cleaning %DIST_DIR% directory
rd /s/q %DIST_DIR%

:end
endlocal
