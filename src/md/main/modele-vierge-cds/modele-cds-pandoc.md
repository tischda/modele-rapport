---
grade: |
	Département Informatique

	5\ieme{} année

	2013 - 2014

document:
	type: "Cahier des Spécifications"
	title: "Caractérisation de plaques d'athérome dans une image échographique"
	short: "Caractérisation de plaques d'athérome"

supervisors:
    category: Encadrant
	list:
		- name: Cyrille FAUCHEUX
		  mail: cyrille.faucheux@etu.univ-tours.fr
	details: "École polytechnique de l'université de Tours"

authors:
    category: Étudiant
	list:
		- name: Daniel TISCHER
		  mail: daniel.tischer@etu.univ-tours.fr
	details: "DI5 2013 - 2014"

emetteur:
    nameEmet: "nom de l'émetteur"
    coordonnees: "EPU-DI"

validators:
	list:
		- name: Cyrille FAUCHEUX
		  dateValide: 20/05/2014
          valide: "O"
          comment: "Ok pour 2013-2014"

modifications:
    list:
        - version: "00"
	      dateModif: 13/02/2012
	      description: "Version initiale : synthèse de la 1ère séance de travail"

preamble:
    main/modele-vierge-cds/include-cds.tex

---

texte
