---
grade: |
	Département Informatique

document:
	type: Manuel de l'utilisateur
	title: Votre rapport avec Pandoc et \LaTeX
	short: Pandoc et \LaTeX
    abstract: |
        Polytech report generation from text sources in Markdown format.

    keywords: LaTeX, Markdown, Pandoc, Template, Report, Polytech
    frenchabstract: |
        Génération de rapports Polytech Tours à partir de fichiers Markdown.

    frenchkeywords: LaTeX, Markdown, Pandoc, Modèle, Rapport, Polytech

supervisors:
    category: Encadrant
	list:
		- name: Cyrille \textsc{Faucheux}
		  mail: cyrille.faucheux@etu.univ-tours.fr
	details: "École polytechnique de l'université de Tours"

authors:
    category: Étudiant
	list:
		- name: Daniel \textsc{Tischer}
		  mail: daniel.tischer@etu.univ-tours.fr
	details: "DI5 2013 - 2014"

preamble:
    include.tex
---

Introduction
===========

Description
-----------

Ce projet présente l'infrastructure nécessaire pour générer un rapport aux
standards de Polytech Tours. Il se base sur la classe de document `EPURapport`
et permet en option de créer le cahier des spécification (CDS).

Les scripts permettent de générer un document PDF à partir des formats sources
\LaTeX &nbsp;et [Markdown](http://daringfireball.net/projects/markdown/). Voici
la chaine de transformation :

![Workflow Markdown &rarr; LaTeX](main/documentation/images/workflow.png)

Cette initiative vient du fait que je ne connaissais aucun des deux outils.
Ainsi, Markdown me paraissait plus souple et plus accessible, mais \LaTeX
&nbsp;n'a pas son pareil pour une présentation de qualité. En combinant les
deux, nous obtenons le meilleur des deux mondes.

Installation
------------
Les dépendances nécessaires pour générer un rapport sont les suivantes :

* [MacTex](http://www.tug.org/mactex/) 2013 pour Mac | [MikTeX](http://miktex.org/download) 2.9.5105 pour Windows

* [Pandoc](http://johnmacfarlane.net/pandoc/installing.html) 1.12.3

Si en plus, vous voulez développer et tester (comparer) la sortie PDF, vous
aurez besoin de :

* bash (Windows: [Cygwin](http://www.cygwin.com/install.html))
* pdfinfo (dans le répertoire `bin` de [msysgit](http://msysgit.github.io/), à placer dans le `PATH` après Cygwin)
* [ImageMagick](http://www.imagemagick.org/script/binary-releases.php#windows) (`ImageMagick-6.8.9-0-Q16-x64-dll.exe`)
* [GhostScript](http://www.ghostscript.com/download/gsdnld.html) (copier `gswin64c.exe` vers `gs.exe`)

La configuration des outils nécessite un peu de travail manuel :

__MacTex__ : Ajouter `/usr/local/texlive/2013/bin/x86_64-darwin` à la variable
PATH. Puis aller dans Applications/Tex et lancer "Tex Live Utility" et mettre à
jour l'installation.

__Pandoc__ s'installe sous Windows dans `%USERPROFILE%` [^1] ce que l'on peut éviter avec :

~~~~
msiexec /i pandoc-1.12.3.msi ALLUSERS=1 APPLICATIONFOLDER="u:\bin\pandoc"
~~~~

[^1]: [Windows - new location?](https://groups.google.com/forum/#!topic/pandoc-discuss/2tNKYA9e1f4)

Prise en main
-------------

### Présentation

Voici un aperçu général de l'organisation du projet :

~~~~
.
+---build                    Résultats de compilation
+---docs                     Documentation en PDF
+---logos                    Autres logos (pour les chercheurs)
+---modele                   EPURapport.cls
+---pandoc                   Template Pandoc
+---root                     Fichiers copiés à la racine de la distribution
+---scripts                  Scripts de compilation
\---src                      Sources et instances de test
    +---md                   Fichiers Markdown
    \---tex                  Fichiers LaTeX
~~~~

Les fichiers les plus importants sont :

 * `EPURapport.cls` : La classe de document de Polytech Tours

 * `pandoc.latex` : Le template de conversion pour Pandoc

 * `make.{bat|sh}` : Le script principal

 * `compile-{md|tex}.{bat|sh}` : Les scripts de compilation


### Ecrire un rapport

On commence par créer une nouvelle structure de documentation. La commande
`dist.{bat|sh}` va générer une arborescence (distribution) sous `./build/dist`
que vous pouvez copier telle que dans votre projet :

~~~
.
+---build                    Résultats de compilation
    \---dist
        +---modele
        |   +---logos
        |   \---packages
        +---pandoc
        +---scripts
        \---src
            +---md
            \---tex
~~~


Pour générer le rapport, il faut ensuite exécuter le script `make.{bat|sh}` à la
racine de `dist` qui compilera les sources `.md` et/ou `.tex` (à vous de
choisir) en fichier PDF.

Le PDF final sera placé dans un nouveau répertoire `./build`. On y trouvera
également les fichiers de log.


Implémentation
==============

Markdown &rarr; LaTeX
---------------------

Pandoc est un outil écrit en
[Haskell](http://www.haskell.org/haskellwiki/Haskell) qui se veut le couteau
suisse de la conversion multi-formats (markdown, reStructuredText, textile,
HTML, DocBook, LaTeX, MediaWiki markup, OPML, ou Haddock markup).

Par exemple, pour générer directement du PDF à partir de \LaTeX, on exécutera :

~~~~
pandoc -o document.pdf doc-utf8.tex
~~~~

... ou de Markdown vers MS-Word :

~~~~
pandoc -o readme.docx README.md
~~~~

Pour appliquer la classe `EPURapport`, il faut modifier le template par défaut
que l'on exporte ainsi :

~~~~
pandoc -D latex > pandoc-default.latex
~~~~

Notre version customisée `./pandoc/pandoc.latex` gère le passage des valeurs
définies dans l'en-tête YAML à la classe `EPURapport`. Une mise à jour de Pandoc
ou de la classe `EPURapport` peut donc nécessiter une révision de ce template.
Pour donner un exemple de la tranformation appliquée, le code suivant :

~~~~
authors:
    category: Étudiant
    list:
        - name: Daniel \textsc{Tischer}
          mail: daniel.tischer@etu.univ-tours.fr
    details: "DI5 2013 - 2014"
~~~~

... devient :

~~~~
\authors{
  \category{Étudiant}{
    {
      \name{Daniel \textsc{Tischer}} \mail{daniel.tischer@etu.univ-tours.fr}
    }
  }
  \details{DI5 2013 - 2014}
}
~~~~


Notre template s'occupe également de l'inclusion du fichier `include.tex`
puisque Markdown ne permet pas d'inclusion dans le préambule.


LaTeX &rarr; PDF
----------------

Une particularité du traitement \LaTeX &nbsp;est la nécessité d'exécuter
plusieurs fois la même commande :

~~~~
$ pdflatex monRapport
$ bibtex biblio
$ pdflatex monRapport
$ pdflatex monRapport
~~~~

Pour générer notre rapport, cela ne doit pas nous préoccuper. Les scripts
fournis pour Mac et Windows répondent aux besoins les plus courants,
c'est-à-dire la création d'une table des matières, d'une bibliographie et d'un
index.

Les deux commandes externes appelées sont :

1. pandoc pour la conversion Markdown &rarr; \LaTeX
2. [latexmk](http://www.ctan.org/pkg/latexmk/) qui se charge d'automatiser la
génération du document

Le paramétrage de ces outils est assez délicat et diverge selon les plateformes
(les fichiers de logs sont vos amis). Même avec Cygwin il faut être attentif à
l'ordre des outils dans le PATH et gérer correctement le format des chemins
(`/cygdrive/u/src` et ';' comme séparateur sous Windows). On peut examiner les
fichiers dans `./scripts` pour plus de détails, mais il n'est pas nécessaire de
les modifier.

Pandoc permet de spécifier les options de la classe de document `EPURapport` en
ligne de commande. Nous faisons cela dans le script principal `make.{bat|sh}`.

Windows:

~~~~
set PANDOC_OPTS=-V classoption=CDS
~~~~

Unix:

~~~~
export PANDOC_OPTS="-V classoption=CDS"
~~~~

Ces options sont ensuite ajoutées à la ligne de commande de Pandoc :

~~~
PANDOCOPTIONS="--template=${BASE_DIR}/pandoc/pandoc.latex \
    --highlight-style=tango \
    --number-sections \
    --chapters \
    --listings \
    -V documentclass=${DOCUMENTCLASS} \
    --filter pandoc-citeproc \
    --standalone ${PANDOC_OPTS}"
~~~

Notons que nous redirigeons la sortie et l'erreur standard sur `/dev/null` car
la sortie de \LaTeX &nbsp; est très verbeuse. Si les logs ne sont pas générés,
il faudra enlever cette redirection dans `make.{bat|sh}`

~~~
./scripts/compile-md.sh ${INPUT} > /dev/null 2>&1
~~~


Notes d'utilisation
===================

Limitations
-----------

Markdown n'a de loin pas toutes les fonctions de \LaTeX, mais il est possible
d'intégrer du code \LaTeX, soit directement _inline_ dans le fichiers Markdown,
soit par un fichier d'inclusion. Dans les deux cas, il sera correctement
interprété par `pdflatex` dans la seconde phase de transformation.

### Citations et références

Le format de citation de Pandoc ne génère pas d'hyperliens vers la
bibliographie[^2]. On utilisera donc du \LaTeX &emsp; inline, et la compilation
avec `bibtex` fera automatiquement le lien avec la bibliographie.

~~~~
\cite{bechtel01}
~~~~

De même, pour référencer une image[^3] ou un tableau[^4] avec Markdown :

~~~~
![Pandoc Logo \label{fg:sys}](img/pandoclogo.png)
...
Reference FIGURE \ref{fg:sys} like this.
~~~~

En revanche, le référencement des sections est immédiat, car Pandoc ajoute
lui-même les Labels.[^5]

~~~~
Voir la section [Références]
~~~~

### Coloration syntaxique

La coloration syntaxique des sources se fait _uniquement_ si le langage est
reconnu par les _deux_ systèmes[^6].

### Pas de Markdown inline

Dans l'exemple suivant, le code Markdown dans l'environnement \LaTeX &nbsp;
n'est pas interprété.

~~~~
\begin{center}
  Markdown [Pandoc](http://johnmacfarlane.net/pandoc/index.html)
\end{center}
~~~~


[^2]: [How generate hyperlink to references in pdf?](https://groups.google.com/forum/#!msg/pandoc-discuss/0UlUwrViCOQ/KBBs-6rNvY0J)
[^3]: [How to reference a figure in pandoc markdown?](https://groups.google.com/forum/#!topic/pandoc-discuss/MxGKvnNI08c)
[^4]: [New Feature: internal links to tables and figures](https://github.com/jgm/pandoc/issues/813)
[^5]: [Automatic Markdown reference links for section titles](https://github.com/jgm/pandoc/issues/691)
[^6]: [LaTeX output from Markdown: lstlisting with custom language](https://github.com/jgm/pandoc/issues/993)


Inclusion de fichiers
---------------------

On peut inclure des fichiers .tex avec `\input{}` et `\include{}`, ils seront
évalués comme du \LaTeX  &emsp; (et non comme du Markdown). Attention aux noms
des fichiers d'inclusion, on peut avoir des surprises en cas de conflit de
nommage avec une librairie standard.

Attention également à ce que l'on met dans `include.tex`, ce fichier est inclus
dans le [Preamble](http://latex.wikia.com/wiki/List_of_LaTeX_commands#Preamble_commands)
(section réservée aux commandes du type `\usepackage{}`).


Chemin d'inclusion
------------------

Les images et fichiers `.tex` référencés dans le code source sont recherchés
relativement au répertoire source (`src/md` pour Markdown et `src/tex` pour
\LaTeX). Ainsi le code suivant :

~~~
![Workflow](main/documentation/images/workflow.png)
~~~

... fera référence à l'image stockée ici :

~~~
u:\src\polytech\modele-rapport\src\md\main\documentation\images\workflow.png
[------------------------------------][------------------------------------]
     chemin source pour Markdown                 chemin relatif
~~~

En pratique, les scripts exportent la variable `TEXINPUTS` qui permet à \LaTeX
&nbsp;de trouver ses classes :

~~~
export TEXINPUTS="${SRC_DIR}:${BUILD_DIR}:${CLS_PATH}//:"
~~~

Composition de fichiers
-----------------------

Pandoc ne connaît pas le concept de fichiers d'inclusion. Les fichiers Markdown
sont combinés à la compilation, *avant* le passage par `pdflatex` (donc avant
l'application de la classe `EPURapport`).

Pour diviser le document en plusieurs fichiers, il faut les specifier dans
l'ordre dans le script `make.{bat|sh}` :

~~~
INPUT="en-tete.md introduction.md chapitre1.md chapitre2.md conclusion.md"
~~~

Avec \LaTeX, en revanche, il faut utiliser `\input{}` (ou `\include{}` pour
commencer sur une nouvelle page) avec un document parent et un document fils par
chapitre. On peut aussi spécifier plusieurs fichiers dans la variable INPUT,
mais ils seront générés comme des documents séparés.


Images et graphiques
--------------------

### PSTricks

Pour que la transformation des graphiques puisse se faire, il faut activer le
paramètre `-enable-write18` (ou `-shell-escape`) . En plus des problèmes de
sécurité, cette option ralentit le traitement (15 &rarr; 23 secondes). La
méthode recommandée (et la seule qui marche pour Pandoc) est d'exécuter d'abord
:

    pdflatex -shell-escape graph.tex

où `graph.tex` est un document `.tex` indépendant qui inclut les directives
PSTricks. La commande génère un PDF nommé `graph-pics.pdf` que l'on importe
ensuite comme image dans le document principal :

~~~
\begin{figure}[!ht]
  \centering
  \includegraphics[width=7cm]{tex/pstricks-pics.pdf}
  \caption{\label{fig: pstricks} PSTricks inserted via LaTeX command}
\end{figure}
~~~

### Taille d'une image

Pandoc ne permet pas de spécifier la taille d'une image, cette dernière est
redimensionnée automatiquement. On peut contourner ce problème en rajoutant de
l'espace de chaque côté de l'image (avec MS-Paint), mais augmenter la taille de
l'image augmente aussi la taille du PDF en sortie.

Une meilleure solution est d'utiliser, comme précédemment, du \LaTeX &nbsp;
inline.


Quelques conseils
-----------------

Au cours de ce projet, j'ai découvert quelques ecueils que l'on peut facilement
éviter :

* les documents Markdown doivent toujours se terminer par un saut ligne

* ne pas mettre de ':' dans le texte YAML (ok avec des guillemets, par exemple :
  "version : 00")

* la taille des sauts de ligne ne sont pas identiques pour \<return\> et \\\\

* les impressions papier sont de meilleure qualité avec Acrobat Reader



Annexes
=======

Editeurs
--------

Il existe de nombreux éditeurs pour Markdown. Personnellement, j'utilise
[Sublime Text](http://www.sublimetext.com/) avec les extensions `LaTeXTools`,
`MarkdownEditing` et `MonokaiExtended` :

![Sublime Text](main/documentation/images/sublime2.png)

J'ai affecté le script `make.bat` à la combinaison de touches `CTRL+B`, je
visualise ensuite le résultat dans [Sumatra
PDF](http://blog.kowalczyk.info/software/sumatrapdf/free-pdf-reader.html)
(détecte les changements automatiquement, contrairement à Acrobat Reader).

On peut aussi proposer [Haroopad](http://pad.haroopress.com/user.html), qui pour
l'instant est en beta, donc à utiliser avec précautions.

Pour \LaTeX, on peut recommander [TexMaker](http://www.xm1math.net/texmaker/) et
[TexStudio](http://texstudio.sourceforge.net/), tous deux disponibles pour
Windows et Mac.

Liens utiles
------------

 * [Reducing the console output of LaTeX](http://tex.stackexchange.com/questions/1191/reducing-the-console-output-of-latex)
 * [Install TEX package in subfolder](http://tex.stackexchange.com/questions/31925/install-package-in-subfolder)
 * [Use TeXShop Preview window when different output-dir is set](http://tex.stackexchange.com/questions/67211/use-texshop-preview-window-when-different-output-dir-is-set)
 * [What is the use of percent signs (%) at the end of lines?](http://tex.stackexchange.com/questions/7453/what-is-the-use-of-percent-signs-at-the-end-of-lines)
 * [Why are URLs typeset with monospace fonts by default?](http://tex.stackexchange.com/questions/53962/why-are-urls-typeset-with-monospace-fonts-by-default)
 * [Tips on Writing a Thesis in LaTeX](http://www.khirevich.com/latex/)

