---
grade: |
	Département Informatique

	5\ieme{} année

	2013 - 2014

document:
	type: Rapport Science de la Décision
	title: Caractérisation de plaques d'athérome dans une image échographique
	short: Caractérisation de plaques d'athérome
    abstract: |
        Résumé en Anglais
        Résumé en Anglais

        Résumé en Anglais (avec saut de ligne)

    keywords: Keywords in English
    frenchabstract: Résumé Rapport
    frenchkeywords: Mots Clés

supervisors:
    category: Encadrant
	list:
		- name: Cyrille \textsc{Faucheux}
		  mail: cyrille.faucheux@etu.univ-tours.fr
	details: "École polytechnique de l'université de Tours"

authors:
    category: Étudiants
	list:
		- name: Thibault MORELLE
		  mail: thibault.morelle@etu.univ-tours.fr
        - name: Rémy PRADIGNAC
          mail: remy.pradignac@etu.univ-tours.fr
	details: "DI5 2013 - 2014"

preamble:
    include.tex
---

Introduction
============

Ce document est écrit en "code page" ANSI (Windows).

Premier chapitre
============

Conclusion
============

\annexes
