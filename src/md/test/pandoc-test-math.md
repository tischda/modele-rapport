Typesetting Math
================

Basic math
----------

The text above is based on a paper by Edward R. Scheinerman[^1]. A few more
examples from mathTeX tutorial[^2].

[^1]: <http://www.ams.jhu.edu/~ers/learn-latex/>
[^2]: <http://www.forkosh.com/mathtex.html>.

$$
e^x=\sum_{n=0}^\infty\frac{x^n}{n!}
$$

$$
e^x=\lim_{n\to\infty} \left(1+\frac xn\right)^n
$$

$$
\varepsilon = \sum_{i=1}^{n-1} \frac1{\Delta x}
  \int\limits_{x_i}^{x_{i+1}} \left\{
    \frac1{\Delta x}\big[
      (x_{i+1}-x)y_i^\ast+(x-x_i)y_{i+1}^\ast
    \big]-f(x)
  \right\}^2dx
$$


<!-- Use \input{} for inline include and \include{} to start on next page -->
\input{test/tex/math}
<!-- Note: this technique only works for LaTeX source, not for Markdown -->


Matrices
--------

A small matrix:

$$
A = \left[\begin{matrix} 3 & 4 & 0 \\ 2 & -1 & \pi \end{matrix}\right] .
$$

A big matrix:

$$
D = \left[
    \begin{matrix}
      \lambda_1 & 0 & 0 & \cdots & 0 \\
      0 & \lambda_2 & 0 & \cdots & 0 \\
      0 & 0 & \lambda_3 & \cdots & 0 \\
      \vdots & \vdots & \vdots & \ddots & \vdots \\
      0 & 0 & 0 & \cdots & \lambda_n
    \end{matrix}
    \right].
$$


See [Wikibook on LaTeX][3] for more examples.

[3]: <http://en.wikibooks.org/wiki/LaTeX/Mathematics#Symbols>
