Creating References
===================

Bibliography
------------
\LaTeX supports bibliogaphy, use this method:

~~~
Dealing with faculty \cite{bechtel01}
~~~

Dealing with faculty \cite{bechtel01}


Index
-----
\LaTeX supports indexes. Add this anywhere in the text (it's an invisible marker):

~~~
\index{Ipsum}
~~~

Lorem ipsum \index{Ipsum} dolor sit amet, consectetur adipiscing elit. Phasellus
commodo condimentum felis dignissim varius. Maecenas sed purus sapien. Curabitur
vestibulum iaculis tellus, sit amet fermentum tellus vulputate eget. Praesent
mauris eros, bibendum vitae tortor id, suscipit suscipit justo. In a venenatis
turpis, sit amet dignissim diam \index{diam}. Interdum et malesuada fames ac
ante ipsum primis in faucibus. Quisque lectus eros, ullamcorper quis porta eu,
congue eget nulla. Sed sapien lectus, malesuada vel euismod sed, rutrum
\index{rutrum} venenatis turpis. Sed pharetra lobortis arcu eget pharetra.
Aenean fringilla fermentum rutrum.


<!-- This is used for laTeX post-processing -->

\bibliography{test/pandoc-test-bibliography}
\bibliographystyle{alphadin}

\addcontentsline{toc}{chapter}{Bibliographie}
\printindex
