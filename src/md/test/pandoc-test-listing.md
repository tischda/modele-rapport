Code listings
=============

This works only with Pandoc:

~~~~~~~~
~~~{caption="Hello world" language=Java}
...
~~~
~~~~~~~~

~~~{caption="Hello world" language=Java}
/**
 * HelloWorld.java
 */

public class HelloWorld
{
	public static void main(String[] args) {
		System.out.println("Hello World!");
	}
}
~~~


This also works, but there is no caption (won't be referenced in listings):

~~~~~~~~
~~~~C
...
~~~~
~~~~~~~~

~~~~C
// C hello world example
#include <stdio.h>

int main()
{
  char string[] = "Hello World";
  printf("%s\n", string);
  return 0;
}
~~~~


Without the language specifier, no caption, no highlighting:

~~~~
#include <stdio.h>

int main()
{
  char string[] = "Hello World";
  printf("%s\n", string);
  return 0;
}
~~~~
