---
grade: |
	Département Informatique

	5\ieme{} année

	2013 - 2014

document:
	type: Test Document
	title: Using Pandoc to Write Great Reports
	short: Using Pandoc
    abstract: |
        This is a sample document for testing a long paragraph line 1
        This is a sample document for testing a long paragraph line 2
        line 3

        this is a sample document for testing line 4

    keywords: sample, document, latex, pandoc
    frenchabstract: ceci est un document de test
    frenchkeywords: exemple, de, mot, clé

supervisors:
    category: Encadrant
	list:
		- name: Cyrille FAUCHEUX
		  mail: cyrille.faucheux@etu.univ-tours.fr
	details: "École polytechnique de l'université de Tours"

authors:
    category: Étudiant
	list:
		- name: Daniel TISCHER
		  mail: daniel.tischer@etu.univ-tours.fr
	details: DI5 2013 - 2014

preamble:
    test/include-test.tex
---


