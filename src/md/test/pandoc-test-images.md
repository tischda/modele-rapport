Images, graphics and layout
===========================

Images
------

### Plain images

~~~
![Pandoc Logo](images/pandoclogo.png)
~~~

![Pandoc Logo](test/images/pandoclogo.png)


### Referencing images

Not possible out of the box, but there is a
[workaround](https://github.com/jgm/pandoc/issues/904). You can reference FIGURE
\ref{fg:sys} like this.

~~~
![Pandoc Logo \label{fg:sys}](images/pandoclogo.png)
~~~

![Pandoc Logo \label{fg:sys}](test/images/pandoclogo.png)


Graphics
--------

Pandoc does not support `-shell-espape`, so here is a workaround : Pre-process
the graphics with \LaTeX:

~~~
pdflatex -shell-escape pstricks.tex
~~~

Then import the graphics the usual way:

~~~
![PSTricks inserted via Markdown](tex/pstricks-pics.pdf)
~~~

![PSTricks inserted via Markdown](test/tex/pstricks-pics.pdf)


or directly with a \LaTeX command:


~~~latex
\begin{figure}[!ht]
  \centering
  \includegraphics[width=7cm]{tex/pstricks-pics.pdf}
  \caption{\label{fig: pstricks} PSTricks inserted via LaTeX command}
\end{figure}
~~~

\begin{figure}[!ht]
  \centering
  \includegraphics[width=7cm]{test/tex/pstricks-pics.pdf}
  \caption{\label{fig: pstricks} PSTricks inserted via LaTeX command}
\end{figure}


Centering
---------

\begin{center}
This is centered
\end{center}

