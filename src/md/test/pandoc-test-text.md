<!-- http://www.unexpected-vortices.com/sw/gouda/quick-markdown-example.html -->

Introduction
============

Here's a link to [a website](http://foo.bar). Here's a link to a [local
doc](local-doc.html). Here's a footnote [^5].

[^5]: Footnote text goes here.

Tables can look like this:

size  material      color
----  ------------  ------------
9     leather       brown
10    hemp canvas   natural
11    glass         transparent

Table: Shoes, their sizes, and what they're made of

(The above is the caption for the table.) Here's a definition list:

apples
  : Good for making applesauce.
oranges
  : Citrus!
tomatoes
  : There's no "e" in tomatoe.

Inline math equations go in like so: $\omega = d\phi / dt$. Display
math should get its own line and be put in in double-dollarsigns:

$$I = \int \rho R^{2} dV$$

Done.

Euro: €

Caractères accentués.

