#!/bin/bash
# -----------------------------------------------------------------------------
# This script copies all the files you need to get started into build/dist
# -----------------------------------------------------------------------------

BUILD_DIR=build
DIST_DIR=${BUILD_DIR}/dist

# -----------------------------------------------------------------------------
# Clean

if [ "$1" = "clean" ] ; then
	echo . Cleaning ${DIST_DIR} directory
	rm -rf ${DIST_DIR}
	exit
fi

[ -d ${DIST_DIR} ] || mkdir -p ${DIST_DIR}

# -----------------------------------------------------------------------------
# Copy

cp -f  root/* ${DIST_DIR}

cp -rf modele ${DIST_DIR}/modele

mkdir -p ${DIST_DIR}/pandoc  && cp -f pandoc/pandoc.latex "$_"
mkdir -p ${DIST_DIR}/scripts && cp -f scripts/* "$_"

mkdir -p ${DIST_DIR}/src/md  && cp -f src/md/include.tex "$_"
mkdir -p ${DIST_DIR}/src/md  && cp -f src/md/main/modele-vierge/* "$_"

mkdir -p ${DIST_DIR}/src/tex && cp -f src/tex/include.tex "$_"
mkdir -p ${DIST_DIR}/src/tex && cp -f src/tex/main/modele-vierge/* "$_"
