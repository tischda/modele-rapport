Modele-rapport
==============

Polytech Tours, 3ème année, 2013-1024

Description
-----------

Voici quelques outils pour générer un rapport aux standards de Polytech Tours.
Il inclut une version récente de la classe de document `EPURapport`.

Les scripts permettent de générer un document PDF à partir des formats sources
\LaTeX et [Markdown](http://daringfireball.net/projects/markdown/). Voici la
chaine de transformation :

![Workflow](https://bitbucket.org/tischda/modele-rapport/raw/master/src/md/main/documentation/images/workflow.png)


Installation
------------
Les dépendances nécessaires pour générer un rapport sont les suivantes :

* [MacTex](http://www.tug.org/mactex/) 2013 pour Mac | [MikTeX](http://miktex.org/download) 2.9.5105 pour Windows

* [Pandoc](http://johnmacfarlane.net/pandoc/installing.html) 1.12.3

Si en plus, vous voulez développer et tester (comparer) la sortie PDF, vous
aurez besoin de :

* bash (Windows: [cygwin](http://www.cygwin.com/install.html))
* pdfinfo (Windows: dans le répertoire _bin_ de [msysgit](http://msysgit.github.io/))
* [ImageMagick](http://www.imagemagick.org/script/binary-releases.php#windows) (_ImageMagick-6.8.9-0-Q16-x64-dll.exe_)
* [GhostScript](http://www.ghostscript.com/download/gsdnld.html) (copier _gswin64c.exe_ vers _gs.exe_)

La configuration des outils nécessite un peu de travail manuel :

__MacTex__ : Ajouter `/usr/local/texlive/2013/bin/x86_64-darwin` à la variable
PATH. Puis aller dans Applications/Tex et lancer "Tex Live Utility" pour mettre à
jour l'installation.

__Pandoc__ s'installe sous Windows dans `%USERPROFILE%`. On peut éviter cela avec :

~~~~
msiexec /i pandoc-1.12.3.msi ALLUSERS=1 APPLICATIONFOLDER="u:\bin\pandoc"
~~~~

Prise en main
-------------

### Présentation

La documentation se trouve dans `docs\documentation-pandoc.pdf`. Voici un
aperçu général de l'organisation du projet :

~~~~
.
+---build                    Resultats de compilation
+---docs                     La documentation en PDF
+---modele                   EPURapport.cls
+---pandoc                   Template Pandoc
+---scripts                  Scripts de compilation
\---src                      Sources et instances de test
    +---md                   Fichiers Markdown
    \---tex                  Fichiers LaTeX
~~~~

Les fichiers les plus importants sont :

 * `EPURapport.cls` : La classe de document de Polytech Tours

 * `pandoc.latex` : Le template de conversion pour Pandoc

 * `make.{bat|sh}` : Le script principal

 * `compile-{md|tex}.{bat|sh}` : Les scripts de compilation


### Ecrire un rapport

On commence par créer une nouvelle structure de documentation. La commande
`dist.{bat|sh}` va générer une arborescence (distribution) sous `build/dist` que
vous pouvez copier telle que dans votre projet.

Pour générer le rapport, il faut ensuite exécuter le script `make.{bat|sh}` qui
compilera les sources `.md` et/ou `.tex` (à vous de choisir) en fichier PDF.

Le PDF final sera placé dans le répertoire `./build`. On y trouvera également
les fichiers de log.

Bonne chance !