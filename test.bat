::-----------------------------------------------------------------------------
:: This script transforms Markdown and LaTeX sources into PDF
::-----------------------------------------------------------------------------
:: Usage:
::   test                  compiles test documents
::   test clean            removes build files
::-----------------------------------------------------------------------------
@echo off
setlocal
set BUILD_DIR=build

if "%1"=="clean" goto clean

if not exist %BUILD_DIR% md %BUILD_DIR%

::-----------------------------------------------------------------------------
:test

set PANDOC_OPTS=-V classoption=UTF8

set FILES=text biblio listing math images

echo . Processing md test files
for %%f in (%FILES%) do (
    echo md: %%f
    call scripts\compile-md.bat test/pandoc-test-%%f.md test/pandoc-header.md > nul 2>&1
)

echo . Processing tex test files
for %%f in (%FILES%) do (
    echo tex: %%f
    copy /y src\tex\test\tex-header.tex + src\tex\test\tex-test-%%f.tex + src\tex\test\tex-footer.tex %BUILD_DIR%\tex-test-%%f.tex > nul
    call scripts\compile-tex.bat ../../%BUILD_DIR%/tex-test-%%f.tex > nul 2>&1
)

for %%f in (%FILES%) do (
    echo . Generating diffs for %%f
    bash scripts/diff-pdf.sh %BUILD_DIR%/pandoc-test-%%f.pdf %BUILD_DIR%/tex-test-%%f.pdf %BUILD_DIR%/diff-%%f.pdf
)

::-----------------------------------------------------------------------------
:template

set PANDOC_OPTS=%PANDOC_OPTS% -V classoption=twoside

echo . Processing pandoc document template
set INPUT=main/modele-vierge/modele-rapport-pandoc.md
call scripts\compile-md.bat %INPUT% > nul 2>&1

echo . Processing latex document template
set INPUT=main/modele-vierge/modele-rapport-tex.tex
call scripts\compile-tex.bat %INPUT% > nul 2>&1

set INPUT=main/modele-vierge-utf8/modele-rapport-tex-utf8.tex
call scripts\compile-tex.bat %INPUT% > nul 2>&1

set PANDOC_OPTS=%PANDOC_OPTS% -V classoption=CDS

echo . Processing pandoc CDS template
set INPUT=main/modele-vierge-cds/modele-cds-pandoc.md
call scripts\compile-md.bat %INPUT% > nul 2>&1

echo . Processing latex CDS template
set INPUT=main/modele-vierge-cds/modele-cds-tex.tex
call scripts\compile-tex.bat %INPUT% > nul 2>&1

bash scripts/diff-pdf.sh %BUILD_DIR%/modele-rapport-pandoc.pdf %BUILD_DIR%/modele-rapport-tex.pdf %BUILD_DIR%/diff-modele-rapport.pdf
bash scripts/diff-pdf.sh %BUILD_DIR%/modele-rapport-pandoc.pdf %BUILD_DIR%/modele-rapport-tex-utf8.pdf %BUILD_DIR%/diff-modele-rapport_utf8.pdf
bash scripts/diff-pdf.sh %BUILD_DIR%/modele-cds-pandoc.pdf %BUILD_DIR%/modele-cds-tex.pdf %BUILD_DIR%/diff-modele-cds.pdf

goto end
::-----------------------------------------------------------------------------
:clean

echo . Cleaning %BUILD_DIR% directory
rd /s/q %BUILD_DIR%

:end
endlocal
