#!/bin/bash
# -----------------------------------------------------------------------------
# This script transforms Markdown and LaTeX sources into PDF
# -----------------------------------------------------------------------------
#  Usage:
#    test                  compiles test documents
#    test clean            removes build files
# -----------------------------------------------------------------------------

BUILD_DIR=build

# -----------------------------------------------------------------------------
# Clean

if [ "$1" = "clean" ] ; then
	echo . Cleaning ${BUILD_DIR} directory
	rm -rf ${BUILD_DIR}
	exit
fi

[ -d ${BUILD_DIR} ] || mkdir ${BUILD_DIR}

# -----------------------------------------------------------------------------
# Test

export PANDOC_OPTS="-V classoption=UTF8"

FILES="text biblio listing math images"

echo . Processing md test files
for f in ${FILES}
do
    echo "md: $f"
    ./scripts/compile-md.sh test/pandoc-test-$f.md test/pandoc-header.md > /dev/null 2>&1
done

echo . Processing tex test files
for f in ${FILES}
do
    echo "tex: $f"
    cat src/tex/test/tex-header.tex src/tex/test/tex-test-$f.tex src/tex/test/tex-footer.tex > ${BUILD_DIR}/tex-test-$f.tex
    ./scripts/compile-tex.sh ../../${BUILD_DIR}/tex-test-$f.tex > /dev/null 2>&1
done

# This requires pdfinfo, ImageMagick and GhostScript
for f in ${FILES}
do
    echo ". Generating diffs for $f"
    ./scripts/diff-pdf.sh ${BUILD_DIR}/pandoc-test-$f.pdf ${BUILD_DIR}/tex-test-$f.pdf ${BUILD_DIR}/diff-$f.pdf
done

# -----------------------------------------------------------------------------
# Template

export PANDOC_OPTS="${PANDOC_OPTS} -V classoption=twoside"

echo . Processing pandoc document template
INPUT="main/modele-vierge/modele-rapport-pandoc.md"
./scripts/compile-md.sh ${INPUT} > /dev/null 2>&1

echo . Processing latex document template
INPUT="main/modele-vierge/modele-rapport-tex.tex"
./scripts/compile-tex.sh ${INPUT} > /dev/null 2>&1

INPUT="main/modele-vierge-utf8/modele-rapport-tex-utf8.tex"
./scripts/compile-tex.sh ${INPUT} > /dev/null 2>&1

export PANDOC_OPTS="${PANDOC_OPTS} -V classoption=CDS"

echo . Processing pandoc CDS template
INPUT="main/modele-vierge-cds/modele-cds-pandoc.md"
./scripts/compile-md.sh ${INPUT} > /dev/null 2>&1

echo . Processing latex CDS template
INPUT="main/modele-vierge-cds/modele-cds-tex.tex"
./scripts/compile-tex.sh ${INPUT} > /dev/null 2>&1

./scripts/diff-pdf.sh ${BUILD_DIR}/modele-rapport-pandoc.pdf ${BUILD_DIR}/modele-rapport-tex.pdf ${BUILD_DIR}/diff-modele-rapport.pdf
./scripts/diff-pdf.sh ${BUILD_DIR}/modele-rapport-pandoc.pdf ${BUILD_DIR}/modele-rapport-tex-utf8.pdf ${BUILD_DIR}/diff-modele-rapport_utf8.pdf
./scripts/diff-pdf.sh ${BUILD_DIR}/modele-cds-pandoc.pdf ${BUILD_DIR}/modele-cds-tex.pdf ${BUILD_DIR}/diff-modele-cds.pdf

